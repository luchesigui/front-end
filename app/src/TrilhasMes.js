import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import Label from './components/Label'
import Title from './components/Title'
import P from './components/Paragraph'
import PanelTab from './components/PanelTab'

import Button from './components/Button'

const DaysWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 40px 0;
`

const TrilhasMes = () => (
  <React.Fragment>
    <Label>Etapa 3</Label>
    <Title>Seus objetivos</Title>
    <P big>Quantas <b>trilhas</b> você gostaria de completar <u>por mês</u>?</P>
    <DaysWrapper>
      <PanelTab profissao="1" type="radio" ghost round>1</PanelTab>
      <PanelTab profissao="2" type="radio" ghost round>2</PanelTab>
      <PanelTab profissao="3" type="radio" ghost round>3</PanelTab>
      <PanelTab profissao="4" type="radio" ghost round>4</PanelTab>
      <PanelTab profissao="5+" type="radio" ghost round>5+</PanelTab>
    </DaysWrapper>
    <Link to="/capitulos-semana"><Button>Continuar</Button></Link>
  </React.Fragment>
)

export default TrilhasMes
