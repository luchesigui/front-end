import React from 'react'
import styled from 'styled-components'

const StyledLabel = styled.span`
  font-family: 'Gotham Rounded Bold';
  font-size: 0.9rem;
  text-transform: uppercase;
  color: #7750fc;
  color: var(--roxo);
`

const Label = ({ children }) => (
  <StyledLabel>{ children }</StyledLabel>
)

export default Label
