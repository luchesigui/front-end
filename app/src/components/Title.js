import React from 'react'
import styled, { css } from 'styled-components'

const StyledTitle = styled.h1`
  font-family: 'Gotham Rounded Bold', 'sans-serif';
  color: #473a57;
  color: var(--roxoEscuro);
  font-size: 2.5rem;
  line-height: 2.4rem;
  text-align: center;
  margin: 5px auto;

  ${
    props => props.big &&
    css`
      margin: 30px auto;
    `
  };
`

const Title = ({ children, big }) => (
  <StyledTitle big={big}>{ children }</StyledTitle>
)

export default Title
