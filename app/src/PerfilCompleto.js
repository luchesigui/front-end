import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import Title from './components/Title'
import Button from './components/Button'
import P from './components/Paragraph'

import perfilCompleto from './assets/img/perfil-completo.png'

const ButtonGroup = styled.div`
  width: 500px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`

const Introducao = () => (
  <React.Fragment>
    <img src={ perfilCompleto } alt="Perfil Completo" />
    <Title big>Perfil <br /> Completo</Title>
    <P>
      Nesse momento iremos falar para o usuário que o perfil inicial de aprendizagem dele está pronto, mas que podemos melhorar ainda mais a personalização do conteúdo dele. Nessa etapa iremos apresentar o nosso organizador de horários, para que a Kolab acompanhe e incentive o usuário a fazer mais tarefas e completar seus capítulos dentro de sua trilha.
    </P>
    <ButtonGroup>
      <Link to="/"><Button ghost>Ver Depois</Button></Link>
      <Link to="/rotina-semana"><Button>Personalizar minha agenda</Button></Link>
    </ButtonGroup>
  </React.Fragment>
)

export default Introducao
