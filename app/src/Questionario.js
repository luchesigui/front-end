import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import store from './redux/store'
import { connect } from 'react-redux'
import { questionarioAction } from './redux/reducers'

import Label from './components/Label'
import Title from './components/Title'
import P from './components/Paragraph'
import PanelTab from './components/PanelTab'
import Button from './components/Button'

import cozinheiro from './assets/img/cozinheiro.png'
import cantora from './assets/img/cantora.png'
import astronalta from './assets/img/astronalta.png'

const PanelWrapper = styled.div`
  --columns: 2;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-columns: repeat(var(--columns), 1fr);
  grid-gap: 12px;
  margin: 40px 0 20px;

  @media (min-width: 568px) {
    --columns: 4;
  }
`

const profissoes = [
  {
    value: 'Astronalta',
    icone: astronalta,
  },
  {
    value: 'Cozinheiro',
    icone: cozinheiro,
  },
  {
    value: 'Cantora',
    icone: cantora,
  },
  {
    value: 'Astronalta 2',
    icone: astronalta,
  },
  {
    value: 'Cozinheiro 2',
    icone: cozinheiro,
  },
  {
    value: 'Cantora 2',
    icone: cantora,
  },
  {
    value: 'Astronalta 3',
    icone: astronalta,
  },
  {
    value: 'Cozinheiro 3',
    icone: cozinheiro,
  },
]

class Questionario extends React.Component {
  updateProfissao = ( event ) => {
    const profissao = event.target.value
    store.dispatch(
      questionarioAction({
        profissao
      })
    )
  }

  render() {
    const { profissaoEscolhida } = this.props

    return (
      <React.Fragment>
        <Label>Etapa 1</Label>
        <Title>Questionário</Title>
        <P big>Selecione o ícone que represente o que você ser quando era criança</P>
        <PanelWrapper onChange={ this.updateProfissao }>
          {
            profissoes &&
            profissoes.map( (profissao, key) => <PanelTab checked={profissao.value === profissaoEscolhida} key={key} profissao={profissao.value} icone={profissao.icone} />)
          }
        </PanelWrapper>
        <Link to="/perfil-completo"><Button>Continuar</Button></Link>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({ questionarioReducer }) => ({
  profissaoEscolhida: questionarioReducer.profissao
});

export default connect(mapStateToProps)(Questionario);
