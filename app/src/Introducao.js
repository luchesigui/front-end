import React from 'react'
import { Link } from 'react-router-dom'

import Title from './components/Title'
import Button from './components/Button'
import P from './components/Paragraph'

import squares from './assets/img/squares.png'

const Introducao = () => (
  <React.Fragment>
    <img src={ squares } alt="Antes de começar" />
    <Title big>Antes de <br /> começar</Title>
    <P>
      Aqui iremos inserir um texto explicando um pouco sobre a metologia que estamos aplicando nesse questionário, para que as pessoas entendam qual é a função e importância dele. Isso irá fazer com que não tenhamos mais problemas de desconfiança com a metodologia ou até mesmo usuários que acreditam que não há necessidade de preencher esse questionário.
    </P>
    <Link to="/questionario"><Button>Ir para questionário</Button></Link>
  </React.Fragment>
)

export default Introducao
