import { combineReducers } from 'redux'

const initialState = {
  profissao: '',
  rotina: {
    semana: [],
    horas: [
      {
        inicio: '',
        fim: '',
      },
    ],
  },
  objetivos: {
    trilhas: '',
    capitulos: '',
  },
}

const questionarioReducer = (state = initialState.profissao, action) => {
  if( action.type === 'ATUALIZA_PROFISSAO' ) {
    return { ...state, ...action.payload }
  }

  return state
}

const rotinaSemanaReducer = ( state = initialState.rotina, action ) => {
  if( action.type === 'ROTINA_SEMANA' ) {
    return { ...state, ...action.payload }
  }

  return state
}

const rotinaHorasReducer = ( state = initialState.rotina, action ) => {
  if( action.type === 'ROTINA_HORAS' ) {
    return { ...state, ...action.payload }
  }

  return state
}

export const questionarioAction = payload => ({ type: 'ATUALIZA_PROFISSAO', payload });
export const rotinaSemanaAction = payload => ({ type: 'ROTINA_SEMANA', payload });
export const rotinaHorasAction = payload => ({ type: 'ROTINA_HORAS', payload });

export default combineReducers({
  questionarioReducer,
  rotinaSemanaReducer,
  rotinaHorasReducer,
})
