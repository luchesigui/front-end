import React from 'react'
import styled from 'styled-components'

const StyledCard = styled.section`
  border-radius: 15px;
  background-color: #fff;
  box-shadow: 2px 4px 8px rgba(0, 0, 0, 0.1), -2px 4px 8px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  padding: 20px;
  margin: 0 auto;
  text-align: center;
  display: block;

  @media (min-width: 770px) {
    width: 770px;
    padding: 60px;
  }
`

const Card = ({ children }) => (
  <StyledCard>{children}</StyledCard>
)

export default Card
