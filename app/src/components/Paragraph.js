import React from 'react'
import styled, { css } from 'styled-components'

const StyledParagraph = styled.p`
  color: #b8afd6;
  text-align: center;
  font-family: 'Gotham Rounded Book';
  line-height: 22px;
  font-size: 0.75rem;

  b {
    color: var(--roxo);
  }

  ${props =>
    props.big &&
    css`
      font-family: 'Gotham Rounded Bold';
      font-size: 1rem;
      letter-spacing: 0.07rem;
    `};
`

const P = ({ children, big }) => (
  <StyledParagraph big={big}>{children}</StyledParagraph>
)

export default P
