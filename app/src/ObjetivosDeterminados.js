import React from 'react'
import { Link } from 'react-router-dom'

import Title from './components/Title'
import Button from './components/Button'
import P from './components/Paragraph'

import concluido from './assets/img/concluido.png'

const ObjetivosDeterminados = () => (
  <React.Fragment>
    <img src={ concluido } alt="Perfil Completo" />
    <Title big>Seus objetivos <br /> foram determinados</Title>
    <P>
      Pronto! Todas as etapas de personalização e acompanhamento foram completadas! Agora fique a vontade para utilizar a plataforma Kolab e conte com a gente para qualquer coisa que precisar. Estaremos ajudando você a completar seus objetivos e a cumprir suas metas de forma didática.
    </P>
    <Link to="/"><Button>Vamos lá</Button></Link>
  </React.Fragment>
)

export default ObjetivosDeterminados
