import React from 'react'
import styled, { css } from 'styled-components'

const StyledPanel = styled.label`
  padding: 10px;
  background-color: #b5a0fb;
  display: inline-block;
  border-radius: 12px;
  filter: contrast(0.4) brightness(1.5);
  transition: filter .15s, box-shadow .15s, backgournd-color .2s, color .2s;
  will-change: filter, box-shadow, background-color, color;
  cursor: pointer;

  &:hover,
  input:checked + & {
    filter: none;
  }

  input:focus + & {
    outline: auto 5px -webkit-focus-ring-color;
  }

  input:checked + & {
    box-shadow:
      2px 4px 8px rgba(0, 0, 0, 0.1),
      -2px 4px 8px rgba(0, 0, 0, 0.1);
  }

  ${
    props => props.ghost &&
    css`
      background-color: transparent;
      padding: 10x;

      input:checked + & {
        background-color: #b5a0fb;
        color: #fff;
      }
    `
  }

  ${
    props => props.round &&
    css`
      --tabSize: 70px;
      font-size: 1.5rem;
      line-height: 3.2rem;
      width: 70px;
      width: var(--tabSize);
      height: 70px;
      height: var(--tabSize);
      border-radius: 50%;
    `
  }
`

const StyledRadio = styled.input`
  position: absolute;
  opacity: 0;
`

const PanelTab = ({
  children,
  profissao,
  icone,
  ghost,
  round,
  type = 'radio',
  checked,
}) => (
  <div style={{position: 'relative'}}>
    <StyledRadio checked={checked} id={`${profissao}`} type={type} name="profissao" value={profissao} readOnly></StyledRadio>
    <StyledPanel ghost={ghost} round={round} htmlFor={`${profissao}`}>
      {
        icone && (
          <img style={{ width: '100%' }} src={icone} alt={`Ícone referente a profissao ${profissao}`} />
        )
      }
      { children }
    </StyledPanel>
  </div>
)

export default PanelTab
